package com.govinda.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.govinda.Model.Mahasiswa;
import com.govinda.R;


import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }
    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa>mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.txtNama.setText(m.getNama());
        // holder.txtNoTelp.setText(m.getNotelp());
        holder.txtNim.setText(m.getNim());
        holder.txtAlamat.setText(m.getNim());
        holder.txtEmail.setText(m.getNim());
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNim, txtNoTelp, txtAlamat, txtEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.tvNama);
            txtNim  = itemView.findViewById(R.id.tvNim);
            //txtNoTelp = itemView.findViewById(R.id.txNoTelp);
            txtAlamat  = itemView.findViewById(R.id.tvAlamat);
            txtEmail  = itemView.findViewById(R.id.tvEmail);
        }
    }
}
