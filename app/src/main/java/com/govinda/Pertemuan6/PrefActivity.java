package com.govinda.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.govinda.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("pref file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

      isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")) {
            btnPref.setText("Logout");
        }
        else {
            btnPref.setText("Login");
        }

        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    Intent intent = new Intent(PrefActivity.this,HomeActivity.class);
                    startActivity(intent);
                }else
                    {
                    editor.putString("isLogin", "0");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });
    }
}