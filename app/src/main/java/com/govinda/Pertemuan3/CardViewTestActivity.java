package com.govinda.Pertemuan3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;


import com.govinda.Adapter.MahasiswaCardAdapter;
import com.govinda.Model.Mahasiswa;
import com.govinda.R;


import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        //data dummy
        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaCardAdapter mahasiswaCardAdapter;
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Sigit Irawan","72180212", "081327183927");
        Mahasiswa m2 = new Mahasiswa("Ronaldo Merani","72180250", "08192782019");
        Mahasiswa m3 = new Mahasiswa("Louis Halawa","72180233", "085218293017");
        Mahasiswa m4 = new Mahasiswa("Govinda Idrusta Sinaga","72180218", "081219283516");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);
    }
}