package com.govinda.Pertemuan5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.govinda.Model.DefaultResult;
import com.govinda.Network.GetDataService;
import com.govinda.Network.RetrofitClientInstance;
import com.govinda.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);
        EditText edNimCari = (EditText)findViewById(R.id.editTextNimCari);
        EditText edNama = (EditText)findViewById(R.id.editTextNama2);
        EditText edNimBaru = (EditText)findViewById(R.id.editTextNim2);
        EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat2);
        EditText edEmail = (EditText)findViewById(R.id.editTextEmail2);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMhs2);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

    btnSimpan.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pd.setTitle("Mohon Menunggu");
            pd.show();

            final EditText edNimCari = (EditText)findViewById(R.id.editTextNimCari);
            final EditText edNama = (EditText)findViewById(R.id.editTextNama2);
            final EditText edNimBaru = (EditText)findViewById(R.id.editTextNim2);
            final EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat2);
            final EditText edEmail = (EditText)findViewById(R.id.editTextEmail2);
            Button btnSimpan = (Button) findViewById(R.id.buttonSimpanMhs2);
            pd = new ProgressDialog(MahasiswaUpdateActivity.this);

            btnSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Mohon Tunggu");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.update_mhs(
                            edNimCari.getText().toString(),
                            edNama.getText().toString(),
                            edNimBaru.getText().toString(),
                            edAlamat.getText().toString(),
                            edEmail.getText().toString(),
                            "72180218"
                    );

                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                        }
                    });


                }
            });

        }
    });
    }
}
