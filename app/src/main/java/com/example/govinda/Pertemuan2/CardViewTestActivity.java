package com.example.govinda.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.govinda.Adapter.MahasiswaCardAdapter;
import com.example.govinda.Model.Mahasiswa;
import com.example.govinda.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rvLatihan);
        MahasiswaCardAdapter mahasiswaRecycleAdapterAdapter;
        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Govinda", "72180218", "085219032277");
        Mahasiswa m2 = new Mahasiswa("Ronaldo", "72180218", "085219030697");
        Mahasiswa m3 = new Mahasiswa("Sigait", "72180218", "085239302277");
        Mahasiswa m4 = new Mahasiswa("Baocah", "72180218", "085219038493");
        Mahasiswa m5 = new Mahasiswa("Louis", "72180218", "085219021349");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecycleAdapterAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaRecycleAdapterAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        rv.setAdapter(mahasiswaRecycleAdapterAdapter);

    }
}